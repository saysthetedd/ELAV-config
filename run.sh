# Create symlinks to config
bash ./createSymLinks.sh

# Install packages
bash ./installPackages.sh

# Success!
echo '
 
    🤘  Sweet. Your machine should be all setup. Reload your terminal to apply changes

'
