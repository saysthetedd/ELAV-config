echoPackage () {
    echo "
    
    📦  Installing $1

    "
}

# Install Homebrew
# https://brew.sh/
if ! [ -x "$(command -v brew)" ];then
    echoPackage 'Homebrew! - https://brew.sh/'
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Install git via Homebrew
# Git comes by default on OSX 
# This gets you the latest version + it works with git automplete
echoPackage 'Git via Homebrew'
brew install git
echoPackage 'Bash Completetion (Makes git autocomplete work)'
brew install bash-completion

# Install Node
if ! [ -x "$(command -v node)" ];then
    echoPackage 'Node'
    brew install node
fi

# Install Pretty Diff
# https://www.npmjs.com/package/pretty-diff
if ! [ -x "$(command -v pretty-diff)" ];then
    echoPackage 'Pretty Diff! - https://www.npmjs.com/package/pretty-diff'
    npm install -g pretty-diff
fi

# Create Sites directory
if ! [ -d ~/Sites ]; then
  mkdir ~/Sites
fi

