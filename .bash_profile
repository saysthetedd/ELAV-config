# Bash Aliases
alias ..='cd ..'
alias cl="clear"
alias ll="ls -lahG"
alias www="cd ~/Sites"

# Git Commands
alias gadd="git add ."
alias glg="git log --graph --pretty='format:%C(red)%d%C(reset) %C(yellow)%h%C(reset) %ar %C(green)%aN%C(reset) %s'"
alias gs="git status"

# Package Aliases
alias pd="pretty-diff"

# Sites
alias wg="www && cd wannago"

# Git Automplete
if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

# Git branch in prompt.
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Custom Prompt
export PS1="\e[0;31m`echo -e '\xE2\x86\x92'`\e[0;36m [\w]\e[m\[\033[0;32m\]\$(parse_git_branch)\[\033[0m\] ▸ "

# Visual Studio Code
code () { VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $* ;}
