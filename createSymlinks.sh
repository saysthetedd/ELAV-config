# create symlink from file in ~/ dir
createSymlink () {
    ln -s -i "$(pwd)"/$1 ~/$1
}

filesToLink=(".bash_profile" ".hyper.js")

echo '

    👉  First we wil create symlinks so that your configuration files can be controlled by this package

'

for i in "${filesToLink[@]}"
do
    createSymlink $i
done

echo '

    ✅  Created Symlinks for configuration files
'
