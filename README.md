# ELAV - Workflow Configuration

This repo sets up a machine with all the packages and configuration I use in my workflow.

### Installation
```
cd ~/
git clone git@gitlab.com:saysthetedd/ELAV-config.git
cd ELAV-config
bash run.sh   
```